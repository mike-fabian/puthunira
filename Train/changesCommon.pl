#!/usr/bin/perl

use utf8;
use strict;
use autodie;
use warnings;
use warnings    qw< FATAL  utf8     >;
use open        qw< :std  :utf8     >;
use charnames   qw< :full >;
use feature     qw< unicode_strings >;
use File::Basename      qw< basename >;
use Carp                qw< carp croak confess cluck >;
use Encode              qw< encode decode >;
use Unicode::Normalize  qw< NFD NFC >;


$^I = '.bak'; # create a backup copy

while (<>) {
s/<.*>//g;  # remove html tags between < and > in Wiki files: 143730
# s/([.?!])(?!\R)/$1\n/g; # add newline after all periods, question marks and exclamation marks
s/(?<![.?!])\n/ /g; # convert newlines that does not follow a period, question mark or exclamation mark to a single space
s/ണ്‍/ൺ/g;  # change old chillu notation \u0D23\u0D4D\u200D to atomic notation \u0D7A: 3
s/ല്‍/ൽ/g;  # change old chillu notation \u0D32\u0D4D\u200D to atomic notation \u0D7D :17
s/ര്‍/ർ/g;  # change old chillu notation \u0D30\u0D4D\u200D to atomic notation \u0D7C : 7
s/ള്‍/ൾ/g; # change old chillu notation \u0D33\u0D4D\u200D to \u0D7E : 5
s/ന്‍റ/ഩ്ഺ/g; # change \u0D28\u0D4D\u200D\u0D31 nta to phonetically correct version \u0D29\u0D4D\u0D3A : 0
s/ൻ്റ/ഩ്ഺ/g;    # change \u0D7B\u0D4D\u0D31 nta to phonetically correct version u0D29\u0D4D\u0D3A : 675
s/ന്റ/ഩ്ഺ/g;    # change \u0D28\u0D4D\u0D31 nta to phonetically correct version \u0D29\u0D4D\u0D3A : 334579
s/ന്‍/ൻ/g;  # change old chillu notation \u0D28\u0D4D\u200D to atomic  notation \u0D7B : 16
s/റ്റ/ഺ്ഺ/g;    # change \u0D31\u0D4D\u0D31 tta to phonetically correct version \u0D3A\u0D4D\u0D3A : 416449
s/ൗ/ൌ/g;  # change  new \u0D57 au date sign to old au \u0D4C : 47150
s/[\h.?!]+/␣/g;  # change consecutive horizontal spaces and common punctuation to openbox
s/[^\p{Block: Malayalam}\n\s\N{U+200C}\N{U+200D}\N{U+2423}]//g; # remove characters other than those specified
s/\N{U+200C}/\N{U+03B6}/g; # ZWNJ with Greek small zeta
s/\N{U+200D}/\N{U+03A3}/g; # ZWJ with Greek capital sigma
s/\p{Cf}//g; # remove control characters
s/^[.?!\n\s]*$//g; # remove empty lines created by above modifications
s/\R+/\n/g; # change consecutive vertical spaces to single newline
print; # print to the modified file
}
