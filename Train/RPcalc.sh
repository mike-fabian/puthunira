#!/bin/bash
total=`awk '{total += $1} END{print total}' $1`
awk '{prop = $1 * 100 / '"$total"'
	cum += prop
	if(val !=$1) {rank++;}; 	
	printf("%d\t%s\t%d\t%2.2f\t%2.2f\n", rank,$2, $1, prop, cum); 
	val = $1}'  $1

