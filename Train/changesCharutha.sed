s/\([^ഗജഡദബഘഝഢധഭ]\)്ര/\1്റ/g;    # change ്ര to ്റ where appropriate
s/\([^തനലസ]\)്ല/\1്ള/g;   # change  ്ല to ്ള where appropriate
s/ഩ്ഺ/ऩ/g;  # change ഩ്ഺ to ऩ
s/ഺ്ഺ/ठ/g; 
s/\([കഖഗഘങചഛജഝഞടഠഡഢണതഥദധനഺഩപഫബഭമയരലവശഷസഹളഴറ]\)്\1/\1ː/g; # replace gemination with special char
s/ൻ/ന⊖/g; # replace ൻ with ന⊖
s/ൺ/ണ⊖/g; # replace ൺ with ണ⊖
s/ർ/റ⊖/g; # replace ർ with റ⊖
s/ൽ/ല⊖/g; # replace ൽ with ല⊖
s/ൾ/ള⊖/g; # replace ൾ with ള⊖
s/ഃ/ഹ⊖/g; # replace ഃ with ഹ⊖
s/ൖ/ഴ⊖/g; # replace ൖ with ഴ⊖
s/ൕ/യ⊖/g; # replace ൕ with യ⊖
s/്/അ/g; # replace ് with അ
s/ാ/ആ/g; # replace ാ with ആ
s/ി/ഇ/g; # replace ി with ഇ
s/ീ/ഈ/g; # replace ീ with ഈ
s/ു/ഉ/g; # replace ു with ഉ
s/ൂ/ഊ/g; # replace ൂ with ഊ
s/ൃ/ഋ/g; # replace ൃ with ഋ
s/െ/എ/g; # replace െ with എ
s/േ/ഏ/g; # replace േ with ഏ
s/ൈ/ഐ/g; # replace ൈ with ഐ
s/ൊ/ഒ/g; # replace ൊ with ഒ
s/ോ/ഓ/g; # replace ോ with ഓ
s/ൌ/ഔ/g; # replace ൌ with ഔ