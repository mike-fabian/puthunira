മലയാളം യൂഩികോഡ് ഘടകങ്ങളുടെ ആവൃത്തി കണക്കാക്കി അതിനനുസരിച്ച് തയ്യാറാക്കിയ കീബോൎഡ് ലേയൗട്ടുകളാണ് പുതുനിരകള്‍. പുതുനിരകളെ ചിട്ടപ്പെടുത്തിയ ശാസ്ത്രീയ രീതി ഈ പഠനറിപ്പോൎട്ടില്‍ ലഭ്യമാണ്. അതിനായി കമ്പ്യൂട്ടറിന് നല്കിയ നിര്‍ദ്ദേശങ്ങളും, തയ്യാറാക്കിയ ഫയലുകളുമാണ് ഈ ഭണ്ഢാരത്തില്‍. 

## ആവൃത്തി കണക്കാക്കല്‍

ആവൃത്തി കണക്കാക്കാനായി ലിനക്സ് ടെർമിനലില്‍ നല്കിയ നിര്‍ദ്ദേശങ്ങളാണ് ചുവടെ നല്കിയിരിക്കുന്നത്.


	cd ~/Desktop/MalayalamKeyboard # change working directory
	cat ~/Desktop/MalayalamWords/corpus-master/* > ./smcCorpus # concatenate all files in the corpus
	wc -lwmc ./smcCorpus # take a word count
	#result 979023  11442986 117010525 305853680 ./smcCorpus
	shuf ./smcCorpus > ./smcCorpusR # shuffle the lines
	wc -lwmc ./smcCorpusR #confirm word count
	# result shows insertion of one empty line break 979024  11442986 117010526 305853681 ./smcCorpusR
	
	./changesCommon.pl ./smcCorpusR # modify the shuffled corpus using the Perl script
	wc -lwmc ./smcCorpusR # repeat word count
	# result 206175    206176 105242702 315161651 ./smcCorpusR
	
	split -l 123705 ./smcCorpusR # split the corpus into two
	mv ./xaa ./Train/trainCorpusOrig # rename the split files meaningfully
	mv ./xab ./Test/testCorpusOrig
	
	rm ./smcCorpus* # remove large files not needed any more.
	wc -lwmc ./Test/testCorpusOrig # check the word count of the test corpus
    # result 82470     82471  42020291 125834275 ./testCorpusOrig
	
	wc -lwmc ./Train/trainCorpusOrig # check the word count of the train corpus
	# result 123705    123705  63222411 189327376 ./trainCorpusOrig
	
	sed -f changesAarambham.sed trainCorpusOrig | awk '{ printf "%s␣", $0 }' > trainCorpusA # apply those changes needed for analysis to develop the Aarambham keyboard
	wc -lwmc ./trainCorpusA # check the word count
	# result 0         1  63222411 189574786 ./trainCorpusA
	
	grep -o . ./trainCorpusA  > leaderA # split the corpus into single letters
	wc -lwmc ./leaderA # take word count
	#result 63222411  63222410 126444822 252797197 ./leaderA
	
	rm ./trainCorpusA # remove the bulky file
	
	sort ./leaderA | uniq -c | sort -k1bnr > charFreqKA # count the frequency of the various characters
	./RPcalc.sh charFreqKA > rpCharKA # calculate percentages and ranks
	
	sed 1d ./leaderA > followerA # removing the first character so that the following character aligns line wise
	wc -lwmc ./followerA # word count
	# result 63222410  63222409 126444820 252797193 ./followerA
	
	paste ./leaderA ./followerA > pairsKA # join together so that each character and the character following it are in one line.
	wc -lwmc ./pairsKA # word count
	# result 63222411 126444819 252889643 505594391 ./pairsKA
	
	sort ./pairsKA | uniq -c | sort -k1bnr > pairFreqKA # take counts of each unique combination of a character and its follower
	./RPpaircalc.sh pairFreqKA > rpBigramKA # calculate percentages and ranks
	
	sed -f ./changesCharutha.sed ./trainCorpusOrig |  awk '{ printf "%s␣", $0 }' > trainCorpusC # apply those changes needed for analysis to develop the Aarambham keyboard
	wc -lwmc ./trainCorpusC # word count
	#result 0         1  60269944 176827321 ./trainCorpusC
	
	grep -o . ./trainCorpusC  > leaderC # split the corpus into single letters
	wc -lwmc ./leaderC # word count
	# result 60269944  60269943 120539888 237097265 ./leaderC
	
	rm ./trainCorpusC # remove the bulky file
	sort leaderC | uniq -c | sort -k1bnr > charFreqKC # count the frequency of the various characters
	./RPcalc.sh charFreqKC > rpCharKC # calculate percentages and ranks
	
	sed 1d ./leaderC > followerC # removing the first character so that the following character aligns line wise
	wc -lwmc ./followerC # word count
	# result 60269943  60269942 120539886 237097261 ./followerC
	
	paste ./leaderC ./followerC > pairsKC # join together so that each character and the character following it are in one line.
	wc -lwmc ./pairsKC # word count
	#result 60269944 120539885 241079775 474194527 ./pairsKC
	
	rm ./follower* # remove unwanted files
	sort ./pairsKC | uniq -c | sort -k1bnr > pairFreqKC # take counts of each unique combination of a character and its follower
	./RPpaircalc.sh pairFreqKC > rpBigramKC # calculate percentages and ranks

മുകളിലെ നിർദ്ദേശങ്ങളില്‍ ഉപയോഗിച്ച changesCommon.pl,changesAarambham.sed, changesCharutha.sed, RPcalc.sh, RPpaircalc.sh എന്നീ സ്ക്രിപ്റ്റ് ഫയലുകളും ഘടകങ്ങളുടെ ആവൃത്തി രേഖപ്പെടുത്തിയിരിക്കുന്ന charFreqKA, charFreqKc, pairFreqKA, pairFreqKC, rpCharKA, rpBigramKA, rpCharKC, rpBigramKC എന്ന ഫയലുകളും Train എന്ന directory-യില്‍ ലഭ്യമാണ്.

## കീബോൎഡ് ലേയൗട്ടുകള്‍

LayoutDefinitions എന്ന directory-യില്‍ പുതുനിരകള്‍ യാഥാർത്ഥ്യമാക്കുന്ന keyboard definitions ആണുള്ളത്. Xkb layout definitions ml_pnA, en_pnEqf, en_pnEqfX എന്ന ഫയലുകളാണ്. Ibus-ില്‍ en-pnEqf.mim, ml-pnC.mim, en-pnEqf.mim.pnEqfX, ml-pnC.mim.pnEqfX  എന്ന ഫയലുകളാണ് ഉപയോഗിക്കേണ്ടത്. Windows-	ില്‍ pnEqf.klc, pnA.klc എന്ന ഫയലുകള്‍ ഉപയോഗിക്കണം. പുതുനിരകളുടെ പ്രത്യേകതകളെ കുറിച്ചറിയാനും അവയിലേത് ഉപയോഗിക്കണം, എങ്ങനെ ഉപയോഗിക്കണം എന്നതറിയാനും ഇവിടെ നോക്കുക.

 SupportFiles എന്ന directory-യില്‍ പ്രധാനമായും ലേയൗട്ടുകള്‍ക്കുള്ള icon ഫയലുകളാണ്. ഇവ png ഫോൎമാറ്റിലും, അവ തയ്യാറാക്കിയ Krita എന്ന സോഫ്റ്റ്‌വേറിന്റെ തനത് ഫോൎമാറ്റിലും ലഭ്യമാക്കിയിട്ടുണ്ട്. കീബോൎഡ് ലേയൗട്ടുകളിലെ non printing keys-ിനെ കാണിക്കുവാനുപയോഗിക്കേണ്ട icon-ുകളും ലഭ്യമാണ്. Icon ഫയലുകള്‍ കൂടാതെ, കീബോൎഡ് ലേയൗട്ട് പ്രിന്റ് ചെയ്യാന്‍ സഹായകരമാകുന്ന ഒരു xlsx ഫയലും ലഭ്യമാണ്.
 
## താരതമ്യം

കീബോൎഡ് ലേയൗട്ടുകളെ താരതമ്യം ചെയാന്‍ ആദ്യപടിയായി ഓരോ ലേയൗട്ടിനനുസരിച്ച് test corpus-ില്‍ മാറ്റം വരുത്തി ആവൃത്തി കണക്കാക്കി. ലിനക്സ് ടെർമിനലില്‍ നല്കിയ നിര്‍ദ്ദേശങ്ങളാണ് ചുവടെ കൊടുത്തിരിക്കുന്നത്.

	cd ./Test # change the working directory
	
	# modify the test corpus for each layout tested
	sed -f changestestpnA.sed ./testCorpusOrig | awk '{ printf "%s␣", $0 }'  > testCorpuspnA
	sed -f changestestpnC.sed ./testCorpusOrig | awk '{ printf "%s␣", $0 }'  > testCorpuspnC
	sed -f changestestrem.sed ./testCorpusOrig | awk '{ printf "%s␣", $0 }'  > testCorpusrem
	sed -f changestestins.sed ./testCorpusOrig | awk '{ printf "%s␣", $0 }'  > testCorpusins
	
	# split each of the corpora into individual letters
	grep -o . ./testCorpuspnA > tleaderpnA
	grep -o . ./testCorpuspnC > tleaderpnC
	grep -o . ./testCorpusins > tleaderins
	grep -o . ./testCorpusrem > tleaderrem
	
	#remove the unwanted files
	rm ./testCorpuspnA ./testCorpuspnC ./testCorpusrem ./testCorpusins
	
	# calculate the frequency
	sort ./tleaderpnA | uniq -c > testFreqpnA
	sort ./tleaderpnC | uniq -c > testFreqpnC
	sort ./tleaderins | uniq -c > testFreqins
	sort ./tleaderrem | uniq -c > testFreqrem
	
	# rearrange to bring a character and the character that follows it in one line
	sed 1d ./tleaderins > tfollowerins
	paste ./tleaderins ./tfollowerins > tpairsins
	sed 1d ./tleaderrem > tfollowerrem
	paste ./tleaderrem ./tfollowerrem > tpairsrem
	sed 1d ./tleaderpnA > tfollowerpnA
	paste ./tleaderpnA ./tfollowerpnA > tpairspnA
	sed 1d ./tleaderpnC > tfollowerpnC
	paste ./tleaderpnC ./tfollowerpnC > tpairspnC
	
	# calculate the frequency of the pairs
	sort ./tpairsins | uniq -c | sort -k1bnr > tpairFreqins
	sort ./tpairsrem | uniq -c | sort -k1bnr > tpairFreqrem
	sort ./tpairspnA | uniq -c | sort -k1bnr > tpairFreqpnA
	sort ./tpairspnC | uniq -c | sort -k1bnr > tpairFreqpnC

അടുത്തപടിയായി R എന്ന സോഫ്റ്റ്‌വേറില്‍ ഈ ആവൃത്തികളെ വിശകലനം ചെയ്തു. ഇതിനുപയോഗിച്ച PuthuNiraCompare.Rmd എന്ന ഫയലും, ആവൃത്തി നിർണ്ണയത്തിനായി test corpus-ില്‍ മാറ്റം വരുത്താനുപയോഗിച്ച changestestrem.sed, changestestins.sed, changestestpnA.sed, changestestpnC.sed എന്ന സ്ക്രിപ്റ്റ് ഫയലുകളും, ആവൃത്തി നിർണ്ണയത്തിന്റെ ഫലങ്ങള്‍ കൊള്ളുന്ന testFreqrem, testFreqins, testFreqpnA, testFreqpnC, tpairFreqrem, tpairFreqins, tpairFreqpnA, tpairFreqpnC എന്ന ഫയലുകളും Test എന്ന directory-യില്‍ ലഭ്യമാണ്. കീബോൎഡ് ലേയൗട്ടുകളുടെ ഘടന R-ില്‍ വിശകലനം ചെയ്യാന്‍  തയ്യാറാക്കിയ tsv ഫയലാണ് layouts.txt. കീബോൎഡിലെ കട്ടകളുടെ സ്ഥാനങ്ങള്‍ക്ക് നല്കിയിട്ടുള്ള ക്രമസംഖ്യകള്‍ rank.txt എന്ന tsv ഫയലില്‍ ലഭ്യമാണ്. Qwerty ലേയൗട്ടില്‍ കീബോൎഡിലെ കട്ടകളുടെ സ്ഥാനങ്ങള്‍ക്ക് നല്കിയിട്ടുള്ള ക്രമസംഖ്യകള്‍ വരച്ച് കാണിക്കാന്‍ വേണ്ടി ഉപയോഗപ്പെടുത്താനാണ് qwerty.txt എന്ന tsv ഫയല്‍. 